const { src, dest, series, parallel } = require('gulp');
const sass = require('gulp-sass');

function css() {
    return src('.src/styles/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(dest('build/css'));
}

exports.default = parallel(css);
